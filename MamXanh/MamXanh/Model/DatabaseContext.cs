﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MamXanh.Model
{
    public class DatabaseContext:DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options):base(options) { }
        public virtual DbSet<Post> Posts { set; get; }
        public virtual DbSet<AccountModel> Accounts { set; get; }

    }
}
