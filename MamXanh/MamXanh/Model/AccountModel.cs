﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MamXanh.Model
{
    public class AccountModel
    {
        [Key]
        [StringLength(50)]
        public string Username { set; get; }
        [StringLength(50)]
        public string Password { set; get; }
    }
}
