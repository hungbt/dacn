// import Vue from 'vue';
import { Component, Vue } from 'vue-property-decorator';
// import Component from 'vue-class-component';

@Component
export default class SlideBarComponent extends Vue {
  menuItem: any[] = [];
  constructor() {
    super();
    this.menuItem = [
      {
        Index: 0,
        Name: 'Home',
        Alias: 'Trang chủ',
        Active: true,
        Path: '/admin',
        Icon: 'fa fa-fw fa-bars'
      }
    ];
    console.log('SlideBarComponent Contructor', this);
  }
  mounted() {
    console.log('mounted', this.$router.currentRoute.path);
    this.menuItem.forEach(
      (n, index) => (n.Active = n.Path === this.$router.currentRoute.path)
    );
    const c = $('#sidebar-menu').find('li.submenu');
    console.log('Test Jquery', c);
  }
  itemClick(e: any) {
    this.menuItem.forEach((n, index) => (n.Active = index === e));
    console.log(this.menuItem);
  }
}
