import Vue from 'vue';
import { Component } from 'vue-property-decorator';

@Component
export default class TopBarComponent extends Vue {
  constructor() {
    super();
    console.log('TopBarComponent Contructor');
  }
  mounted() {
    console.log('mounted');
  }
  logout() {
    localStorage.removeItem('tk');
    localStorage.removeItem('mk');
    sessionStorage.removeItem('tk');
    sessionStorage.removeItem('mk');
    this.$router.push('/');
  }
}
