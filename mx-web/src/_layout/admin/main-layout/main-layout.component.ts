import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import '../../../app.scss';
import '../../../assets/css/font-awesome/css/font-awesome.css';
import '../../../assets/css/style.css';

import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import SlideBarComponent from '../../../_layout/admin/page-slide-bar/page-slide-bar.component';
import TopBarComponent from '../../../_layout/admin/page-top-bar/page-top-bar.component';

@Component({
  components: {
    ['slider-bar']: SlideBarComponent,
    ['top-bar']: TopBarComponent
  }
})
export default class MainLayoutComponent extends Vue {
  leftExplain = true;
  constructor() {
    super();
    console.log('MainLayoutComponent Contructor');
  }
  mounted() {
    console.log('mounted MainLayout');
  }
}
