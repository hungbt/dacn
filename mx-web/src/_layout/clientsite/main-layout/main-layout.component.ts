import { Vue, Component } from 'vue-property-decorator';
import '../assets/vendor/nucleo/css/nucleo.css';
import '../assets/vendor/font-awesome/css/font-awesome.min.css';
import '../assets/css/argon.css?v=1.0.1';

@Component
export default class LayoutClientComponent extends Vue {}

/*
  <link href="./assets/img/brand/favicon.png" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="./assets/vendor/nucleo/css/nucleo.css" rel="stylesheet">
  <link href="./assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <!-- Argon CSS -->
  <link type="text/css" href="./assets/css/argon.css?v=1.0.1" rel="stylesheet">
  <!-- Docs CSS -->
  <link type="text/css" href="./assets/css/docs.min.css" rel="stylesheet">
*/
