import Vue from 'vue';
import VueRouter from 'vue-router';
import Helloword from './components/HelloWorld.vue';
import MainLayoutComponent from '@/_layout/admin/main-layout/main-layout.component';
import LayoutClientComponent from './_layout/clientsite/main-layout/main-layout.component';
Vue.use(VueRouter);

const routes = [
  { path: '/', component: LayoutClientComponent },
  {
    path: '/admin',
    component: MainLayoutComponent,
    children: [{ path: 'home', component: Helloword }]
  }
];

export default new VueRouter({
  mode: 'hash',
  routes
});
